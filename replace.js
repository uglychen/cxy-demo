var imgs = $('img[src]');
var replaceReg = /([^|]*)\|(\d*)x(\d*)/;

imgs.each(function(i,v){
    var info = $(v).attr('src');
    var str,width,height;
    if(replaceReg.test(info)){
        info.replace(replaceReg,function(a,s,w,h){
            str = s;
            width = w;
            height = h;
        })

        var cav = $('<canvas>').get(0);
        var Q = cav.getContext('2d');
        cav.width = width;
        cav.height = height;
        Q.fillStyle = '#80f022';
        Q.fillRect(0,0,width,height);
        Q.fillStyle = '#000';
        Q.font="20px Georgia";
        var w = Q.measureText(str).width;
        Q.fillText(str,(width-w)/2,height/2);
        var src = cav.toDataURL();
        $(v).attr('src',src);
    }
})