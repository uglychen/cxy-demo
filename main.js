$(document).ready(function(){
    var roller = new Roller($('.banner-roller'));
})

var Roller = function($c){
    this.root = $c;
    this.slider = this.root.find('.slider');
    this.dots = this.root.find('.dots');
    this.imgs = this.slider.find('img');

    this.count = this.imgs.length;
    this.width = this.root.width();
    this.height = this.root.height();

    this.index = 0;

    this.transTo = function(index){
        if(index<0||index>this.count-1)return;
        this.index = index;
        this.slider.css('transform','translate(-%dpx,0)'.replace('%d',this.width*index));
    }

    var init = function(){
        var _this = this;
        this.imgs.width(this.width).height(this.height);
        this.slider.width(this.width*this.count);
        for(var i=0;i<this.count;i++){
            this.dots.append($('<div>'));
        };

        this.dots.on('mouseover','div',function(e){
            var index = _this.dots.find('div').index(e.target);
            _this.transTo(index);
        })
    };
    init.bind(this)();
}